// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDTceoIjdPbE1L2u5tIvTjMAJ8r_vt2TtQ',
    authDomain: 'fir-fotos-curso.firebaseapp.com',
    databaseURL: 'https://fir-fotos-curso.firebaseio.com',
    projectId: 'fir-fotos-curso',
    storageBucket: 'fir-fotos-curso.appspot.com',
    messagingSenderId: '377135840196',
    appId: '1:377135840196:web:3bc8d3cdbf4af93d5a8291'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

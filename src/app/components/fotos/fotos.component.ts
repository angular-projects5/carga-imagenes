import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Title, Meta } from '@angular/platform-browser';

export interface Item { nombre: string; url: string; }

@Component({
  selector: 'app-fotos',
  templateUrl: './fotos.component.html',
  styles: []
})
export class FotosComponent implements OnInit, OnDestroy {

  private itemsCollection: AngularFirestoreCollection<Item>;
  items: Observable<Item[]>;
  constructor(
    private afs: AngularFirestore,
    private title: Title,
    private meta: Meta
  ) {
    this.title.setTitle('SSR- Chante');
    this.meta.addTag({ name: 'page.info', content: 'fotos page' });
    this.itemsCollection = afs.collection<Item>('img');
    this.items = this.itemsCollection.valueChanges();
  }
  ngOnDestroy(): void {
    this.meta.removeTag('name=\'page.info\'');
  }

  ngOnInit(): void {
  }

}

import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { FileItem } from '../models/file-item';

@Injectable({
  providedIn: 'root'
})
export class CargaImagenesService {

  private CARPETA_IMAGENES = 'img';
  constructor(
    private angularFirestore: AngularFirestore) { }

  private guardarImagen(imagen: any) {
    this.angularFirestore.collection(`/${this.CARPETA_IMAGENES}`)
      .add(imagen);
  }

  cargarImagenerFirebase(imagenes: FileItem[]) {
    const storageRef = firebase.storage().ref();
    imagenes.map(imagen => {
      imagen.estaSubiendo = true;
      if (imagen.progreso >= 100) {
        return;
      }

      const uploadTask: firebase.storage.UploadTask =
        storageRef.child(`${this.CARPETA_IMAGENES}/${imagen.nombreArchivo}`)
          .put(imagen.archivo);

      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot: firebase.storage.UploadTaskSnapshot) => imagen.progreso = (snapshot.bytesTransferred / snapshot.totalBytes) * 100,
        (error) => console.error('Error al subir', error),
        async () => {
          console.log('Imagen cargada correctamente');
          imagen.url = await storageRef.child(`${this.CARPETA_IMAGENES}/${imagen.nombreArchivo}`).getDownloadURL();
          this.guardarImagen({
            nombre: imagen.nombreArchivo,
            url: imagen.url
          });

        });
    });
  }
}

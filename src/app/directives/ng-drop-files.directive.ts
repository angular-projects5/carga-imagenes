import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { FileItem } from '../models/file-item';

@Directive({
  selector: '[appNgDropFiles]'
})
export class NgDropFilesDirective {
  @Input() archivos: FileItem[] = [];

  @Output() mouseSobre = new EventEmitter<boolean>(false);

  constructor() { }

  @HostListener('dragover', ['$event'])
  public onDragEnter(event: any) {
    this.mouseSobre.emit(true);
    this.prevenirDetener(event);
  }
  @HostListener('dragleave', ['$event'])
  public onDragLeave(event: any) {
    this.mouseSobre.emit(false);
  }
  @HostListener('drop', ['$event'])
  public onDrop(event: any) {

    const transferencia = this.getTransferencia(event);

    if (!transferencia) {
      return;
    }

    this.extraerArchivos(transferencia.files);

    this.prevenirDetener(event);

    this.mouseSobre.emit(false);
  }

  private getTransferencia(event: any) {
    return event.dataTransfer ? event.dataTransfer : event.originalEvent.dataTransfer;
  }

  private extraerArchivos(archivosLista: FileList) {
    // tslint:disable-next-line: forin
    for (const propiedad in Object.getOwnPropertyNames(archivosLista)) {
      const archivoTemporal = archivosLista[propiedad];
      if (this.archivoPuedeSerCargado(archivoTemporal)) {
        const nuevoArchivo = new FileItem(archivoTemporal);
        this.archivos.push(nuevoArchivo);
      }
    }
  }

  // Validaciones
  private archivoPuedeSerCargado(archivo: File): boolean {
    return !this.archivoYaAdjuntado(archivo.name) && this.esImagen(archivo.type);
  }
  // Para que no abra las imágenes que se dropean
  private prevenirDetener(event) {
    event.preventDefault();
    event.stopPropagation();
  }

  private archivoYaAdjuntado(nombreArchivo: string): boolean {
    return this.archivos.some(archivo => archivo.nombreArchivo === nombreArchivo);
  }

  private esImagen(tipoArchivo: string): boolean {
    return !tipoArchivo || tipoArchivo === '' ? false : tipoArchivo.startsWith('image');
  }

}
